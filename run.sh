#!/usr/bin/env sh

while [ ! -f /certs/privkey.pem ]
do
  echo "Sleeping 2 seconds waiting for /certs/privkey.pem"
  sleep 2
done

while [ ! -f /certs/fullchain.pem ]
do
  echo "Sleeping 2 seconds waiting for /certs/fullchain.pem"
  sleep 2
done

echo "SSL Certs exist. testing DNS and connection to redis"
pingResult=`redis-cli -h redis ping`
if [ "$pingResult" != "PONG" ]
then
  echo "ping failed. exiting..."
  exit
fi


echo "SSL Certs exist. Starting..."

/usr/sbin/apachectl start

lastSave=`redis-cli -h redis get restart-apache`
lastSaveHard=`redis-cli -h redis get hard-restart-apache`

while true
do
  latestLastSave=`redis-cli -h redis get restart-apache`
  latestLastSaveHard=`redis-cli -h redis get hard-restart-apache`

  if [ "$lastSave" != "$latestLastSave" ]
  then
    echo "Redis has been updated since last check"
    lastSave=$latestLastSave
    /usr/sbin/apachectl graceful
  fi

  if [ "$lastSaveHard" != "$latestLastSaveHard" ]
  then
    echo "Redis has been hard updated since last check"
    exit
  fi

  sleep 3600
done
