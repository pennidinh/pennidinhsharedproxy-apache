FROM debian:stretch

RUN  apt-get update && apt-get --yes install apache2 php redis-tools ruby

COPY /apacheDocuments /var/www/html/
COPY /apacheCgi /var/www/cgi-bin/
COPY /apacheConfig /etc/apache2/

RUN a2enmod headers

COPY /run.sh /

CMD /run.sh
