#!/usr/bin/ruby

require 'cgi'

cgi = CGI.new

pennidinhProxyAddressCookies = cgi.cookies['pennidinhProxyAddress']
pennidinhProxyAddressCookie = pennidinhProxyAddressCookies.length != 1 ? nil : pennidinhProxyAddressCookies[0]
pennidinhProxyAddressFromCookie = pennidinhProxyAddressCookie.nil? ? nil : pennidinhProxyAddressCookie.to_s

expiringTokenParams = cgi.params['expiringToken']
expiringTokenFromParam = expiringTokenParams.length == 1 ? expiringTokenParams[0] : nil

pennidinhProxyAddressParams = cgi.params['pennidinhProxyPort']
pennidinhProxyAddressFromParam = pennidinhProxyAddressParams.length == 1 ? pennidinhProxyAddressParams[0] : nil

cookies = []

if pennidinhProxyAddressFromCookie != pennidinhProxyAddressFromParam
  cookies << CGI::Cookie.new('name'     => 'expiringToken',
                             'value'    => expiringTokenFromParam.nil? ? [] : [expiringTokenFromParam],
                             'path'     => '/',
                             'secure'   => true,
                            )
  cookies << CGI::Cookie.new('name'     => 'pennidinhProxyAddress',
                             'value'    => [pennidinhProxyAddressFromParam],
                             'secure'   => true,
                             'path'     => '/',
                            )
elsif !expiringTokenFromParam.nil?
  cookies << CGI::Cookie.new('name'     => 'expiringToken',
                             'value'    => [expiringTokenFromParam],
                             'path'     => '/',
                             'secure'   => true,
                            )
end

require 'uri'

redirectTo = cgi.params['redirectTo']

redirectToDecoded = redirectTo.length != 1 ? nil : redirectTo[0]

if redirectToDecoded.nil? || redirectToDecoded == ''
  cgi.out do
    "Need a 'redirectTo' parameter set to a non-empty value!"
  end
  return
end

cgi.out('cookie' => cookies, 'status' => 'REDIRECT', 'location' => redirectToDecoded) do
  "cookie value = #{pennidinhProxyAddressFromCookie}. param value #{pennidinhProxyAddressFromParam}. redirect value #{redirectToDecoded}."
end
